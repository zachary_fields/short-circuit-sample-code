#include "SPI.h"
  /************/
 /* SPI PINS */
/************/
#define SCK  13
#define MISO 12
#define MOSI 11
#define SS   10

// The following #define statements have been taken directly from the datasheet

  /****************/
 /* CONTROL BYTE */
/****************/
// Slave Address      XXXX      (number beneath X's)
#define SLAVE_ADDR  0b01000000

// Wired Logic Address    XXX   (number beneath X's)
#define LOGIC_ADDR  0b00000000

// Read/Write Command        X  (number beneath X's)
#define CTL_WRITE   0b00000000
#define CTL_READ    0b00000001

  /**********************/
 /* REGISTER ADDRESSES */
/**********************/
#define REG_IODIRA 0x00
#define REG_IODIRB 0x01
#define REG_GPIOA  0x12
#define REG_GPIOB  0x13

  /*************/
 /* GPIO MASK */
/*************/
// This bitmask will set all 8 I/O pins of one bank to output (0)
#define MASK_ALL_OUTPUT 0b00000000

byte control_byte = (SLAVE_ADDR | LOGIC_ADDR | CTL_WRITE);  // This operation takes all three elements and merges them together (i.e. 01000000)

void setup (void) {  
  /*
   * The MCP23S17 has two GPIO banks, A and B
   * Each GPIO bank has eight GPIO pins
   * The banks are controlled by the on-chip registers
   * The registers take a single byte as a bit mask to set the corresponding pins to input or output
   * By default the registers IODIRA and IODIRB are set to input (i.e. 11111111)
   */
  
  // Initialize SPI "Slave Select" (SS) line
  pinMode(SS, OUTPUT);
  digitalWrite(SS, HIGH);
  
  // Initialize SPI (sets SCK, MOSI, MISO)
  SPI.begin();
  
  // Set GPIO bank A to OUTPUT
  digitalWrite(SS, LOW);
  SPI.transfer(control_byte);
  SPI.transfer(REG_IODIRA);
  SPI.transfer(MASK_ALL_OUTPUT);
  digitalWrite(SS, HIGH);
  
  // Set GPIO bank B to OUTPUT
  digitalWrite(SS, LOW);
  SPI.transfer(control_byte);
  SPI.transfer(REG_IODIRB);
  SPI.transfer(MASK_ALL_OUTPUT);
  digitalWrite(SS, HIGH);
}

void loop (void) {
  static byte io_pin = 0;
  unsigned short bitmask = 0x0000;  // represents two bytes
  unsigned char *spi_byte_array;
  
  // Constrain io_pin from zero to fifteen
  io_pin = io_pin % 16;
  
  // Set pin on bitmask, by bit-shifting 1 (on) to the correct position
  bitmask = 1 << io_pin;
  
  // Point byte array at the bitmask
  spi_byte_array = (unsigned char *)&bitmask;
  
  /*
   * By default the registers for banks A and B are tied together
   * Writing more than eight bits into bank A will result in the overflow going into bank B
   * Here we are writing sixteen bits (two bytes) to bank A, which loads A and B simultaneously
   */
  digitalWrite(SS, LOW);
  SPI.transfer(control_byte);
  SPI.transfer(REG_GPIOA);
  SPI.transfer(spi_byte_array[0]);
  SPI.transfer(spi_byte_array[1]);
  digitalWrite(SS, HIGH);
  
  // Increment io_pin
  io_pin = io_pin + 1;
  
  // Wait for the human eye to catch up
  delay(150);
}

